package br.com.itau;

public class Conta {
    private Cliente cliente;
    private int agencia;
    private double saldo;

    public Conta(Cliente cliente, int agencia, double saldo) {
        this.cliente = cliente;
        this.agencia = agencia;
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double depositar (double valor){
        return saldo += valor;
    }

    public double sacar (double valor){
        return saldo -= valor;
    }
}
